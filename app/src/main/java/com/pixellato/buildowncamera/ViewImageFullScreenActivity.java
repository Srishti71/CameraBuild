package com.pixellato.buildowncamera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.icu.text.DateFormat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.lang.reflect.Field;

public class ViewImageFullScreenActivity extends AppCompatActivity {

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image_full_screen);
        Bitmap bitmap;
        byte[] byteArray = getIntent().getByteArrayExtra("ImageView");
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        Log.i("I am in ViewImage","Class");
        ImageView imageFullScreen = (ImageView) findViewById(R.id.imageFullScreen);
        imageFullScreen.setImageBitmap(bitmap);
    }
}
