package com.pixellato.buildowncamera;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    ListSingleElement arrayAdapter;
    ArrayList<Drawable> drawables = new ArrayList<Drawable>();
    String[] nameOfFiles =  new String[100];
    int countNumberFiles = 0;
    Drawable drawableImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
       // ActionBar actionBar = getActionBar();
       // actionBar.setDisplayHomeAsUpEnabled(true);

        final ArrayList<String> imageName = new ArrayList<>();

            File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "MyCameraApp");
            for (File file : directory.listFiles()) {
                if (file.isFile()) {
                    drawableImage =Drawable.createFromPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)+"/MyCameraApp/"+file.getName());
                    drawables.add(drawableImage);

                    nameOfFiles[countNumberFiles] = file.getName();
                    Log.i("FileName:::", file.getName());
                    countNumberFiles++;
                    imageName.add(file.getName());
                }
            }

        final ListView listView = (ListView)  findViewById(R.id.listOfImage);
        arrayAdapter = new ListSingleElement(this, drawables, imageName);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Bitmap bitmap = ((BitmapDrawable) drawables.get(position)).getBitmap();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] buteArray = byteArrayOutputStream.toByteArray();

                Intent intent = new Intent(Main2Activity.this, ViewImageFullScreenActivity.class);
                intent.putExtra("ImageView",buteArray);

                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                    final PopupMenu popupMenu = new PopupMenu(Main2Activity.this, listView, Gravity.NO_GRAVITY, R.attr.actionDropDownStyle, 0);

                popupMenu.getMenuInflater().inflate(R.menu.menu_shareordelete, popupMenu.getMenu());

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            if(id == R.id.delete){
                                drawables.remove(position);
                                imageName.remove(position);
                                arrayAdapter.notifyDataSetChanged();
                            }

                            if(id == R.id.share){
                                item = popupMenu.getMenu().findItem(id);
                                ShareActionProvider shareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
                                Bitmap bitmap = ((BitmapDrawable) drawables.get(position)).getBitmap();
                                String path = MediaStore.Images.Media.insertImage(Main2Activity.this.getContentResolver(),
                                        bitmap, "Design", null);
                                Uri uri = Uri.parse(path);

                                Intent shareIntent = new Intent();
                                shareIntent.setAction(Intent.ACTION_SEND);
                                shareIntent.setType("image/*");
                                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);

                                shareActionProvider.setShareIntent(shareIntent);
                            }
                            return true;
                        }
                    });

                popupMenu.show();
                return true;
            }
        });
    }
    class ListSingleElement extends ArrayAdapter{

        ArrayList listOfFileName = new ArrayList();
        private final Activity context;
        ArrayList<Drawable> listOfImages = new ArrayList();
        public  ListSingleElement(Activity context, ArrayList listImage, ArrayList list){
            super(context, R.layout.activity_list__single, list);
            this.context = context;
           listOfFileName = list;
           listOfImages = listImage;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent){

            LayoutInflater inflater = context.getLayoutInflater();
            view = inflater.inflate(R.layout.activity_list__single, null, true);

            Log.i("Position "+position,listOfFileName.get(position)+"");
            TextView textView =  view.findViewById(R.id.imageName);
            textView.setText(listOfFileName.get(position)+"");

            ImageView imageView =  view.findViewById(R.id.imageVIew);
            imageView.setImageDrawable(listOfImages.get(position));
            return view;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.home){
            this.finish();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
